unit gicv3;

{$L gic}

interface

procedure Init_GICv3(GICDAddress: UInt64; GICRAddress: UInt64);

implementation

uses
    sysutils, math, mmio, utils, aarch64;

{$WRITEABLECONST OFF}
const
    ID_AA64PFR0_GIC_MASK  = UInt64($F);
    ID_AA64PFR0_GIC_SHIFT = UInt64(24);

    // GIC Distributor interface general definitions

    // Constants to categorize interrupts
    MIN_SGI_ID     = UInt32(0);
    MIN_SEC_SGI_ID = UInt32(8);
    MIN_PPI_ID     = UInt32(16);
    MIN_SPI_ID     = UInt32(32);
    MAX_SPI_ID     = UInt32(1019);

    TOTAL_SPI_INTR_NUM  = UInt32(MAX_SPI_ID - MIN_SPI_ID + 1);
    TOTAL_PCPU_INTR_NUM = UInt32(MIN_SPI_ID - MIN_SGI_ID);

    // Mask for the priority field common to all GIC interfaces
    GIC_PRI_MAX = UInt32($FF);

    // Mask for the configuration field common to all GIC interfaces
    GIC_CFG_MASK = UInt32($3);

    // Constant to indicate a spurious interrupt in all GIC interfaces
    GIC_SPURIOUS_INTERRUPT = UInt32(1023);

    // Interrupt configurations: 2-bit fields with LSB reserved
    GIC_INTR_CFG_LEVEL = UInt32(0 shl 1);
    GIC_INTR_CFG_EDGE  = UInt32(1 shl 1);

    // Highest possible interrupt priorities
    GIC_HIGHEST_SEC_PRIORITY = UInt32($00);
    GIC_HIGHEST_NS_PRIORITY  = UInt32($80);

    // Common GIC Distributor interface register offsets
    GICD_CTLR       = UInt32($0);
    GICD_TYPER      = UInt32($4);
    GICD_IIDR       = UInt32($8);
    GICD_IGROUPR    = UInt32($80);
    GICD_ISENABLER  = UInt32($100);
    GICD_ICENABLER  = UInt32($180);
    GICD_ISPENDR    = UInt32($200);
    GICD_ICPENDR    = UInt32($280);
    GICD_ISACTIVER  = UInt32($300);
    GICD_ICACTIVER  = UInt32($380);
    GICD_IPRIORITYR = UInt32($400);
    GICD_ICFGR      = UInt32($c00);
    GICD_NSACR      = UInt32($e00);

    // GICD_CTLR bit definitions
    CTLR_ENABLE_G0_SHIFT = UInt32(0);
    CTLR_ENABLE_G0_MASK  = UInt32($1);
    CTLR_ENABLE_G0_BIT   = UInt32(1 shl CTLR_ENABLE_G0_SHIFT);

    // Common GIC Distributor interface register constants
    PIDR2_ARCH_REV_SHIFT = UInt32(4);
    PIDR2_ARCH_REV_MASK  = UInt32($F);

    // GIC revision as reported by PIDR2.ArchRev register field
    ARCH_REV_GICV1 = UInt32($1);
    ARCH_REV_GICV2 = UInt32($2);
    ARCH_REV_GICV3 = UInt32($3);
    ARCH_REV_GICV4 = UInt32($4);

    IGROUPR_SHIFT    = UInt32(5);
    ISENABLER_SHIFT  = UInt32(5);
    ICENABLER_SHIFT  = UInt32(ISENABLER_SHIFT);
    ISPENDR_SHIFT    = UInt32(5);
    ICPENDR_SHIFT    = UInt32(ISPENDR_SHIFT);
    ISACTIVER_SHIFT  = UInt32(5);
    ICACTIVER_SHIFT  = UInt32(ISACTIVER_SHIFT);
    IPRIORITYR_SHIFT = UInt32(2);
    ITARGETSR_SHIFT  = UInt32(2);
    ICFGR_SHIFT      = UInt32(4);
    NSACR_SHIFT      = UInt32(4);

    // GICD_TYPER shifts and masks
    TYPER_IT_LINES_NO_SHIFT = UInt32(0);
    TYPER_IT_LINES_NO_MASK  = UInt32($1F);

    // Value used to initialize normal world interrupt priorities, four at a time
    GICD_IPRIORITYR_DEF_VAL = UInt32(GIC_HIGHEST_NS_PRIORITY or
                                     (GIC_HIGHEST_NS_PRIORITY shl 8) or (GIC_HIGHEST_NS_PRIORITY shl 16) or
                                     (GIC_HIGHEST_NS_PRIORITY shl 24));

    // Interrupt group definitions
    INTR_GROUP1S  = UInt32(0);
    INTR_GROUP0   = UInt32(1);
    INTR_GROUP1NS = UInt32(2);

    // Interrupt IDs reported by the HPPIR and IAR registers 
    PENDING_G1S_INTID  = UInt32(1020);
    PENDING_G1NS_INTID = UInt32(1021);

    // Constant to categorize LPI interrupt 
    MIN_LPI_ID = UInt32(8192);

    // GICv3 can only target up to 16 PEs with SGI
    GICV3_MAX_SGI_TARGETS = UInt32(16);

    // PPIs INTIDs 16-31
    MAX_PPI_ID = UInt32(31);

    // Total number of GICv3 PPIs
    TOTAL_PRIVATE_INTR_NUM = UInt32(TOTAL_PCPU_INTR_NUM);

    // Total number of GICv3 SPIs
    TOTAL_SHARED_INTR_NUM = UInt32(TOTAL_SPI_INTR_NUM);

// SGIs: 0-15, PPIs: 16-31
function IS_SGI_PPI(ID: UInt32): Boolean; inline;
begin
    IS_SGI_PPI := (ID >= MIN_SPI_ID) and (ID <= MAX_SPI_ID);
end;

function GIC_REV(R, P: UInt32): UInt32; inline;
begin
    GIC_REV := ((R shl 4) or P);
end;

const
    // GICv3 and 3.1 specific Distributor interface register offsets and constants
    GICD_TYPER2      = UInt32($0C);
    GICD_STATUSR     = UInt32($10);
    GICD_SETSPI_NSR  = UInt32($40);
    GICD_CLRSPI_NSR  = UInt32($48);
    GICD_SETSPI_SR   = UInt32($50);
    GICD_CLRSPI_SR   = UInt32($58);
    GICD_IGRPMODR    = UInt32($d00);
    GICD_IGROUPRE    = UInt32($1000);
    GICD_ISENABLERE  = UInt32($1200);
    GICD_ICENABLERE  = UInt32($1400);
    GICD_ISPENDRE    = UInt32($1600);
    GICD_ICPENDRE    = UInt32($1800);
    GICD_ISACTIVERE  = UInt32($1a00);
    GICD_ICACTIVERE  = UInt32($1c00);
    GICD_IPRIORITYRE = UInt32($2000);
    GICD_ICFGRE      = UInt32($3000);
    GICD_IGRPMODRE   = UInt32($3400);
    GICD_NSACRE      = UInt32($3600);

    // GICD_IROUTER<n> register is at $6000 + 8n, where n is the interrupt ID
    // and n >= 32, making the effective offset as $6100
    GICD_IROUTER  = UInt32($6000);
    GICD_IROUTERE = UInt32($8000);

    GICD_PIDR0_GICV3 = UInt32($FFE0);
    GICD_PIDR1_GICV3 = UInt32($FFE4);
    GICD_PIDR2_GICV3 = UInt32($FFE8);

    IGRPMODR_SHIFT = UInt32(5);

    // GICD_CTLR bit definitions
    CTLR_ENABLE_G1NS_SHIFT = UInt32(1);
    CTLR_ENABLE_G1S_SHIFT  = UInt32(2);
    CTLR_ARE_S_SHIFT       = UInt32(4);
    CTLR_ARE_NS_SHIFT      = UInt32(5);
    CTLR_DS_SHIFT          = UInt32(6);
    CTLR_E1NWF_SHIFT       = UInt32(7);
    GICD_CTLR_RWP_SHIFT    = UInt32(31);

    CTLR_ENABLE_G1NS_MASK = UInt32($1);
    CTLR_ENABLE_G1S_MASK  = UInt32($1);
    CTLR_ARE_S_MASK       = UInt32($1);
    CTLR_ARE_NS_MASK      = UInt32($1);
    CTLR_DS_MASK          = UInt32($1);
    CTLR_E1NWF_MASK       = UInt32($1);
    GICD_CTLR_RWP_MASK    = UInt32($1);

    // GICD_IROUTER shifts and masks
    IROUTER_SHIFT     = UInt32(0);
    IROUTER_IRM_SHIFT = UInt32(31);
    IROUTER_IRM_MASK  = UInt32($1);

    GICV3_IRM_PE  = UInt32($0);
    GICV3_IRM_ANY = UInt32($1);

    NUM_OF_DIST_REGS = UInt32(30);

    // GICD_TYPER shifts and masks
    TYPER_ESPI       = UInt32(1 shl 8);
    TYPER_DVIS       = UInt32(1 shl 18);
    TYPER_ESPI_RANGE_MASK = UInt32($1F);
    TYPER_ESPI_RANGE_SHIFT = UInt32(27);
    // I think? The ARM source has:
    //  TYPER_ESPI_RANGE  = UInt32(TYPER_ESPI_MASK << TYPER_ESPI_SHIFT)
    // but TYPER_ESPI_MASK and TYPER_ESPI_SHIFT are not defined
    TYPER_ESPI_RANGE = UInt32(TYPER_ESPI_RANGE_MASK shl TYPER_ESPI_RANGE_SHIFT);


    // Common GIC Redistributor interface registers & constants
    GICR_V4_PCPUBASE_SHIFT = UInt32($12);
    GICR_V3_PCPUBASE_SHIFT = UInt32($11);
    GICR_SGIBASE_OFFSET = UInt32(65536);  // 64 KB
    GICR_CTLR       = UInt32($0);
    GICR_IIDR       = UInt32($04);
    GICR_TYPER      = UInt32($08);
    GICR_STATUSR    = UInt32($10);
    GICR_WAKER      = UInt32($14);
    GICR_PROPBASER  = UInt32($70);
    GICR_PENDBASER  = UInt32($78);
    GICR_IGROUPR0   = UInt32(GICR_SGIBASE_OFFSET + $80);
    GICR_ISENABLER0 = UInt32(GICR_SGIBASE_OFFSET + $100);
    GICR_ICENABLER0 = UInt32(GICR_SGIBASE_OFFSET + $180);
    GICR_ISPENDR0   = UInt32(GICR_SGIBASE_OFFSET + $200);
    GICR_ICPENDR0   = UInt32(GICR_SGIBASE_OFFSET + $280);
    GICR_ISACTIVER0 = UInt32(GICR_SGIBASE_OFFSET + $300);
    GICR_ICACTIVER0 = UInt32(GICR_SGIBASE_OFFSET + $380);
    GICR_IPRIORITYR = UInt32(GICR_SGIBASE_OFFSET + $400);
    GICR_ICFGR0     = UInt32(GICR_SGIBASE_OFFSET + $c00);
    GICR_ICFGR1     = UInt32(GICR_SGIBASE_OFFSET + $c04);
    GICR_IGRPMODR0  = UInt32(GICR_SGIBASE_OFFSET + $d00);
    GICR_NSACR      = UInt32(GICR_SGIBASE_OFFSET + $e00);

    GICR_IGROUPR   = UInt32(GICR_IGROUPR0);
    GICR_ISENABLER = UInt32(GICR_ISENABLER0);
    GICR_ICENABLER = UInt32(GICR_ICENABLER0);
    GICR_ISPENDR   = UInt32(GICR_ISPENDR0);
    GICR_ICPENDR   = UInt32(GICR_ICPENDR0);
    GICR_ISACTIVER = UInt32(GICR_ISACTIVER0);
    GICR_ICACTIVER = UInt32(GICR_ICACTIVER0);
    GICR_ICFGR     = UInt32(GICR_ICFGR0);
    GICR_IGRPMODR  = UInt32(GICR_IGRPMODR0);

    // GICR_CTLR bit definitions
    GICR_CTLR_UWP_SHIFT    = UInt32(31);
    GICR_CTLR_UWP_MASK     = UInt32($1);
    GICR_CTLR_UWP_BIT      = UInt32(1 shl GICR_CTLR_UWP_SHIFT);
    GICR_CTLR_DPG1S_SHIFT  = UInt32(26);
    GICR_CTLR_DPG1S_MASK   = UInt32($1);
    GICR_CTLR_DPG1S_BIT    = UInt32(1 shl GICR_CTLR_DPG1S_SHIFT);
    GICR_CTLR_DPG1NS_SHIFT = UInt32(25);
    GICR_CTLR_DPG1NS_MASK  = UInt32($1);
    GICR_CTLR_DPG1NS_BIT   = UInt32(1 shl GICR_CTLR_DPG1NS_SHIFT);
    GICR_CTLR_DPG0_SHIFT   = UInt32(24);
    GICR_CTLR_DPG0_MASK    = UInt32($1);
    GICR_CTLR_DPG0_BIT     = UInt32(1 shl GICR_CTLR_DPG0_SHIFT);
    GICR_CTLR_RWP_SHIFT    = UInt32(3);
    GICR_CTLR_RWP_MASK     = UInt32($1);
    GICR_CTLR_RWP_BIT      = UInt32(1 shl GICR_CTLR_RWP_SHIFT);
    GICR_CTLR_EN_LPIS_BIT  = UInt32(1 shl 0);

    // GICR_WAKER bit definitions
    WAKER_CA_SHIFT = UInt32(2);
    WAKER_PS_SHIFT = UInt32(1);

    WAKER_CA_MASK = UInt32($1);
    WAKER_PS_MASK = UInt32($1);

    WAKER_CA_BIT = UInt32(1 shl WAKER_CA_SHIFT);
    WAKER_PS_BIT = UInt32(1 shl WAKER_PS_SHIFT);

    // GICR_TYPER bit definitions
    TYPER_AFF_VAL_SHIFT  = UInt32(32);
    TYPER_PROC_NUM_SHIFT = UInt32(8);
    TYPER_LAST_SHIFT     = UInt32(4);
    TYPER_VLPI_SHIFT     = UInt32(1);

    TYEPR_AFF_VAL_MASK  = UInt32($FFFFFFFF);
    TYPER_PROC_NUM_MASK = UInt32($FFFF);
    TYPER_LAST_MASK     = UInt32($1);

    TYPER_LAST_BIT = UInt32(1 shl TYPER_LAST_SHIFT);
    TYPER_VLPI_BIT = UInt32(1 shl TYPER_VLPI_SHIFT);

    // GICR_IIDR bit definitions
    IIDR_PRODUCT_ID_MASK   = UInt32($FF);
    IIDR_VARIANT_MASK      = UInt32($F);
    IIDR_REV_MASK          = UInt32($F);
    IIDR_IMPLEMENTER_MASK  = UInt32($FFF);
    IIDR_PRODUCT_ID_SHIFT  = UInt32(24);
    IIDR_VARIANT_SHIFT     = UInt32(16);
    IIDR_REV_SHIFT         = UInt32(12);
    IIDR_IMPLEMENTER_SHIFT = UInt32(0);
    IIDR_PRODUCT_ID_BIT    = UInt32(1 shl IIDR_PRODUCT_ID_SHIFT);
    IIDR_VARIANT_BIT       = UInt32(1 shl IIDR_VARIANT_SHIFT);
    IIDR_REV_BIT           = UInt32(1 shl IIDR_REV_SHIFT);
    IIDR_IMPLEMENTER_BIT   = UInt32(1 shl IIDR_IMPLEMENTER_SHIFT);

    IIDR_MODEL_MASK = UInt32(IIDR_PRODUCT_ID_MASK shl IIDR_PRODUCT_ID_SHIFT or
                             IIDR_IMPLEMENTER_MASK shl IIDR_IMPLEMENTER_SHIFT);

    GIC_PRODUCT_ID_GIC600   = UInt32($2);
    GIC_PRODUCT_ID_GIC600AE = UInt32($3);
    GIC_PRODUCT_ID_GIC700   = UInt32($4);

    // Note that below revisions and variants definitions are per GIC600/GIC600AE
    // specification
    GIC_REV_P0 = UInt32($1);
    GIC_REV_P1 = UInt32($3);
    GIC_REV_P2 = UInt32($4);
    GIC_REV_P3 = UInt32($5);
    GIC_REV_P4 = UInt32($6);
    GIC_REV_P6 = UInt32($7);

    GIC_VARIANT_R0 = UInt32($0);
    GIC_VARIANT_R1 = UInt32($1);
    GIC_VARIANT_R2 = UInt32($2);

    // GICv3 and 3.1 CPU interface registers & constants

    // ICC_SRE bit definitions
    ICC_SRE_EN_BIT  = UInt32(1 shl 3);
    ICC_SRE_DIB_BIT = UInt32(1 shl 2);
    ICC_SRE_DFB_BIT = UInt32(1 shl 1);
    ICC_SRE_SRE_BIT = UInt32(1 shl 0);

    // ICC_IGRPEN1_EL3 bit definitions
    IGRPEN1_EL3_ENABLE_G1NS_SHIFT = UInt32(0);
    IGRPEN1_EL3_ENABLE_G1S_SHIFT  = UInt32(1);

    IRGRPEN1_EL3_ENABLE_G1NS_BIT = UInt32(1 shl IGRPEN1_EL3_ENABLE_G1NS_SHIFT);
    IRGRPEN1_EL3_ENABLE_G1S_BIT  = UInt32(1 shl IGRPEN1_EL3_ENABLE_G1S_SHIFT);

    // ICC_GRPEN0_EL1 bit definitions
    IGRPEN1_EL1_ENABLE_G0_SHIFT = UInt32(0);
    IGRPEN1_EL1_ENABLE_G0_BIT   = UInt32(1 shl IGRPEN1_EL1_ENABLE_G0_SHIFT);

    // ICC_HPPIR0_EL1 bit definitions
    HPPIR0_EL1_INTID_SHIFT = UInt32(0);
    HPPIR0_EL1_INTID_MASK  = UInt32($FFFFFF);

    // ICC_HPPIR1_EL1 bit definitions
    HPPI10_EL1_INTID_SHIFT = UInt32(0);
    HPPIR1_EL1_INTID_MASK  = UInt32($FFFFFF);

    // ICC_IAR0_EL1 bit definitions
    IAR0_EL1_INTID_SHIFT = UInt32(0);
    IAR0_EL1_INTID_MASK  = UInt32($FFFFFF);

    // ICC_IAR1_EL1 bit definition
    IAR1_EL1_INTID_SHIFT = UInt32(0);
    IAR1_EL1_INTID_MASK  = UInt32($FFFFFF);

    // ICC SGI definitions
    SGIR_TGT_MASK    = UInt64($FFFF);
    SGIR_AFF1_SHIFT  = UInt32(16);
    SGIR_INTID_SHIFT = UInt32(24);
    SGIR_INTID_MASK  = UInt64($F);
    SGIR_AFF2_SHIFT  = UInt32(32);
    SGIR_IRM_SHIFT   = UInt32(40);
    SGIR_IRM_MASK    = UInt64($1);
    SGIR_AFF3_SHIFT  = UInt32(48);
    SGIR_AFF_MASK    = UInt64($FF);

    SGIR_IRM_TO_AFF = UInt32(0);

function GICV3_SGIR_VALUE(Aff3, Aff2, Aff1: UInt64; IntID, IRM, TGT: UInt32): UInt64; inline;
begin
    GICV3_SGIR_VALUE := (((Aff3 and SGIR_AFF_MASK) shl SGIR_AFF3_SHIFT) or
        ((IRM and SGIR_IRM_MASK) shl SGIR_IRM_SHIFT) or
        ((Aff2 and SGIR_AFF_MASK) shl SGIR_AFF2_SHIFT) or
        ((IntID and SGIR_INTID_MASK) shl SGIR_INTID_SHIFT) or
        ((Aff1 and SGIR_AFF_MASK) shl SGIR_AFF1_SHIFT) or
        (TGT and SGIR_TGT_MASK));
end;

const
    // GICv3 and 3.1 ITS registers and constants
    GITS_CTLR    = UInt32($0);
    GITS_IIDR    = UInt32($4);
    GITS_TYPER   = UInt32($8);
    GITS_CBASER  = UInt32($80);
    GITS_CWRITER = UInt32($88);
    GITS_CREADR  = UInt32($90);
    GITS_BASER   = UInt32($100);

    // GITS_CTLR bit definitions
    GITS_CTLR_ENABLED_BIT   = UInt32(1 shl 0);
    GITS_CTLR_QUIESCENT_BIT = UInt32(1 shl 1);

    GITS_TYPER_VSGI = UInt64(1 shl 39);

type
    TGICv3_IRQ_Group = (GICV3_G1S, GICV3_G1NS, GICV3_G0);

const
    INT_ID_MASK = UInt32($FFFFFF);

{-
#define DIV_ROUND_UP_2EVAL(n, d)	(((n) + (d) - 1) / (d))

#define GICD_NUM_REGS(reg_name)	\
	DIV_ROUND_UP_2EVAL(TOTAL_SHARED_INTR_NUM, (1 << reg_name##_SHIFT))

#define GICR_NUM_REGS(reg_name)	\
	DIV_ROUND_UP_2EVAL(TOTAL_PRIVATE_INTR_NUM, (1 << reg_name##_SHIFT))
-}

const
    GICR_NUM_REGS_IGROUPR    = ((TOTAL_PRIVATE_INTR_NUM + (1 shl IGROUPR_SHIFT - 1) div (1 shl IGROUPR_SHIFT)));
    GICR_NUM_REGS_ISENABLER  = ((TOTAL_PRIVATE_INTR_NUM + (1 shl ISENABLER_SHIFT - 1) div (1 shl ISENABLER_SHIFT)));
    GICR_NUM_REGS_ISPENDR    = ((TOTAL_PRIVATE_INTR_NUM + (1 shl ISPENDR_SHIFT - 1) div (1 shl ISPENDR_SHIFT)));
    GICR_NUM_REGS_ISACTIVER  = ((TOTAL_PRIVATE_INTR_NUM + (1 shl ISACTIVER_SHIFT - 1) div (1 shl ISACTIVER_SHIFT)));
    GICR_NUM_REGS_IPRIORITYR = ((TOTAL_PRIVATE_INTR_NUM + (1 shl IPRIORITYR_SHIFT - 1) div (1 shl IPRIORITYR_SHIFT)));
    GICR_NUM_REGS_ICFGR      = ((TOTAL_PRIVATE_INTR_NUM + (1 shl ICFGR_SHIFT - 1) div (1 shl ICFGR_SHIFT)));
    GICR_NUM_REGS_IGRPMODR   = ((TOTAL_PRIVATE_INTR_NUM + (1 shl IGRPMODR_SHIFT - 1) div (1 shl IGRPMODR_SHIFT)));

    GICD_NUM_REGS_IGROUPR    = ((TOTAL_SHARED_INTR_NUM + (1 shl IGROUPR_SHIFT - 1) div (1 shl IGROUPR_SHIFT)));
    GICD_NUM_REGS_ISENABLER  = ((TOTAL_SHARED_INTR_NUM + (1 shl ISENABLER_SHIFT - 1) div (1 shl ISENABLER_SHIFT)));
    GICD_NUM_REGS_ISPENDR    = ((TOTAL_SHARED_INTR_NUM + (1 shl ISPENDR_SHIFT - 1) div (1 shl ISPENDR_SHIFT)));
    GICD_NUM_REGS_ISACTIVER  = ((TOTAL_SHARED_INTR_NUM + (1 shl ISACTIVER_SHIFT - 1) div (1 shl ISACTIVER_SHIFT)));
    GICD_NUM_REGS_IPRIORITYR = ((TOTAL_SHARED_INTR_NUM + (1 shl IPRIORITYR_SHIFT - 1) div (1 shl IPRIORITYR_SHIFT)));
    GICD_NUM_REGS_ICFGR      = ((TOTAL_SHARED_INTR_NUM + (1 shl ICFGR_SHIFT - 1) div (1 shl ICFGR_SHIFT)));
    GICD_NUM_REGS_IGRPMODR   = ((TOTAL_SHARED_INTR_NUM + (1 shl IGRPMODR_SHIFT - 1) div (1 shl IGRPMODR_SHIFT)));
    GICD_NUM_REGS_NSACR      = ((TOTAL_SHARED_INTR_NUM + (1 shl NSACR_SHIFT - 1) div (1 shl NSACR_SHIFT)));

type
    mpidr_hash_fn = function (mpidr : UIntPtr): UInt32;

    pinterrupt_prop = ^interrupt_prop;
    interrupt_prop = record
                         intr_num: 0..8191;
                         intr_pri: 0..255;
                         intr_grp: 0..3;
                         intr_cfg: 0..3;
                     end;

    pgicv3_driver_data = ^gicv3_driver_data;
    gicv3_driver_data = record
                            gicd_base           : UIntPtr;
                            gicr_base           : UIntPtr;
                            interrupt_props     : pinterrupt_prop;
                            interrupt_props_num : UInt32;
                            rdistif_num         : UInt32;
                            rdistif_base_addrs  : UIntPtr;
                            mpidr_to_core_pos   : mpidr_hash_fn;
                        end;

    pgicv3_redist_ctx = ^gicv3_redist_ctx;
    gicv3_redist_ctx = record
                           gicr_propbaser  : UInt64;
                           gicr_pendbaser  : UInt64;
                           gicr_ctlr       : UInt32;
                           gicr_igroupr    : array [0..GICR_NUM_REGS_IGROUPR] of UInt32;
                           gicr_isenabler  : array [0..GICR_NUM_REGS_ISENABLER] of UInt32;
                           gicr_ispendr    : array [0..GICR_NUM_REGS_ISPENDR] of UInt32;
                           gicr_isactiver  : array [0..GICR_NUM_REGS_ISACTIVER] of UInt32;
                           gicr_ipriorityr : array [0..GICR_NUM_REGS_IPRIORITYR] of UInt32;
                           gicr_icfgr      : array [0..GICR_NUM_REGS_ICFGR] of UInt32;
                           gicr_igrpmodr   : array [0..GICR_NUM_REGS_IGRPMODR] of UInt32;
                           gicr_nascr      : UInt32;
                       end;

    pgicv3_dist_ctx = ^gicv3_dist_ctx;
    gicv3_dist_ctx = record
                         gicd_irouter    : array [0..TOTAL_SHARED_INTR_NUM] of UInt64;
                         gicd_ctlr       : UInt32;
                         gicd_igroupr    : array [0..GICD_NUM_REGS_IGROUPR] of UInt32;
                         gicd_isenabler  : array [0..GICD_NUM_REGS_ISENABLER] of UInt32;
                         gicd_ispendr    : array [0..GICD_NUM_REGS_ISPENDR] of UInt32;
                         gicd_isactiver  : array [0..GICD_NUM_REGS_ISACTIVER] of UInt32;
                         gicd_ipriorityr : array [0..GICD_NUM_REGS_IPRIORITYR] of UInt32;
                         gicd_icfgr      : array [0..GICD_NUM_REGS_ICFGR] of UInt32;
                         gicd_igrpmodr   : array [0..GICD_NUM_REGS_IGRPMODR] of UInt32;
                         gicd_nascr      : array [0..GICD_NUM_REGS_NSACR] of UInt32;
                     end;

    pgicv3_its_ctx = ^gicv3_its_ctx;
    gicv3_its_ctx = record
                        gits_cbaser  : UInt64;
                        gits_cwriter : UInt64;
                        gits_baser   : array [0..7] of UInt64;
                        gits_ctlr    : UInt32;
                    end;

procedure dsbishst; external name '_dsbishst';

{$I gicv3regs.inc}

function gicv3_redist_size(TypeR_Val: UInt64): UIntPtr; inline;
begin
    {$IFDEF GIC_ENABLE_V4_EXTN}
    if ((TypeR_Val and TYPER_VLPI_BIT) <> 0) then
    begin
        gicv3_redist_size := UIntPtr(1 shl GICR_V4_PCPUBASE_SHIFT);
    end else begin
        gicv3_redist_size := UIntPtr(1 shl GICR_V3_PCPUBASE_SHIFT);
    end;
    {$ELSE}
    gicv3_redist_size := UIntPtr(UInt32(1) shl GICR_V3_PCPUBASE_SHIFT);
    {$ENDIF}
end;

function gicv3_is_intr_id_special_identifier(ID: UInt16): Boolean; inline;
begin
    gicv3_is_intr_id_special_identifier := (ID >= PENDING_G1S_INTID) and (ID <= GIC_SPURIOUS_INTERRUPT);
end;

// Helper GICv3 and 3.1 functions for SEL1

function gicv3_acknowledge_interrupt_sel1: UInt32; inline;
begin
    gicv3_acknowledge_interrupt_sel1 := Read_ICC_IAR1_EL1() and IAR1_EL1_INTID_MASK;
end;

function gicv3_get_pending_interrupt_id_sel1: UInt32; inline;
begin
    gicv3_get_pending_interrupt_id_sel1 := Read_ICC_HPPIR1_EL1() and HPPIR1_EL1_INTID_MASK;
end;

procedure gicv3_end_of_interrupt_sel1(ID : UInt32); inline;
begin
    dsbishst;
    Write_ICC_EOIR1_EL1(ID);
end;

// Helper functions for EL3

function gicv3_acknowledge_interrupt: UInt32; inline;
begin
    gicv3_acknowledge_interrupt := Read_ICC_IAR0_EL1() and IAR0_EL1_INTID_MASK;
end;

procedure gicv3_end_of_interrupt(ID : UInt32); inline;
begin
    dsbishst;
    Write_ICC_EOIR0_EL1(ID);
end;

// Initializes the GICv3 driver in EL3
procedure gicv3_driver_init(plat_driver_data : pgicv3_driver_data);
var
    gic_version  : UInt32;
    gicv2_compat : UInt32;
begin
    KernAssert((plat_driver_data <> NIL), 'plat_driver_data not NIL');
    KernAssert((plat_driver_data^.gicd_base <> 0), 'gicd_base not 0');
    KernAssert((plat_driver_data^.rdistif_num <> 0), 'rdistif_num not 0');
    KernAssert((plat_driver_data^.rdistif_base_addrs <> 0), 'rdistif_base_addrs not 0');

    KernAssert(IS_IN_EL3, 'Must be in EL3');
end;

// continuing on

procedure SetSystemRegister(Value: UInt64); external Name '_set_system_register';
procedure SpinLock(Addr: UIntPtr); external Name '_spin_lock';

procedure Setup_GICv3;
begin
    KernAssert((Read_ID_AA64PFR0_EL1 and (ID_AA64PFR0_GIC_MASK shl ID_AA64PFR0_GIC_SHIFT) <> 0),
           'expecting system register support');
end;


procedure Init_GICv3(GICDAddress: UInt64; GICRAddress: UInt64);
begin
    Setup_GICv3;
end;


end.
