unit utils;

interface

procedure Debug(Item: String); inline;
procedure Debug(Item: LongWord); inline;
procedure DebugLn(Item: String); inline;
procedure DebugLn(Item: LongWord); inline;
procedure DebugLn; inline;
function PadLeft(const S: String; N: Integer): String;
procedure KernAssert(Check : Boolean; Message: String);

implementation

procedure Debug(Item: String); inline;
begin
{$IFDEF KERNDEBUG}
    Write(Item);
{$ENDIF}
end;


procedure Debug(Item: LongWord); inline;
begin
    {$IFDEF KERNDEBUG}
    Write(Item);
    {$ENDIF}
end;


procedure DebugLn(Item: String); inline;
begin
    {$IFDEF KERNDEBUG}
    WriteLn(Item);
    {$ENDIF}
end;


procedure DebugLn(Item: LongWord); inline;
begin
    {$IFDEF KERNDEBUG}
    WriteLn(Item);
    {$ENDIF}
end;


procedure DebugLn; inline;
begin
    {$IFDEF KERNDEBUG}
    WriteLn;
    {$ENDIF}
end;

function PadLeft(const S: String; N: Integer): String;
var
    I: Integer;
    R: String = '';
begin
    for I := 0 to N do
    begin
        R := R + S;
    end;

    PadLeft := R;
end;

procedure KernAssert(Check : Boolean; Message: String);
begin
    if not Check then
    begin
        Write('Kernel assertion failed: ');
        WriteLn(Message);
        while True do
        begin
        end;
    end;
end;

end.
