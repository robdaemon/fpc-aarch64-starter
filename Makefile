CFLAGS=-g -O0 -ffreestanding -mcpu=cortex-a72
FPCFLAGS=-Mobjfpc -FEout -Foout -gw -Paarch64 -Tembedded -XPaarch64-none-elf- -Ch268435456 -dKERNDEBUG -Sa
LINKFLAGS=-k-Tlink.ld
QEMU_DEFAULT=-M virt,gic-version=3,virtualization=true,secure=true -cpu cortex-a72 -m 1G -smp 1 -semihosting -kernel out/generic.elf \
	-serial mon:stdio -nographic -d cpu_reset,guest_errors,int

OBJS = out/start.o \
	out/vectors.o \
	out/gic.o \

UNITS = out/mmio.ppu \
	out/MemoryStream.ppu \
	out/dtb.ppu \
	out/exceptions.ppu \
	out/gicv3.ppu \

.PHONY: all clean run

default: all

all: out/generic.elf

generate: gicv3regs.inc gicv3regs.h

gicv3regs.inc: gic_registers.csv
	cat gic_registers.csv | ruby gen-gic-pascal.rb > gicv3regs.inc

gicv3regs.h: gic_registers.csv
	cat gic_registers.csv | ruby gen-gic-asm.rb > gicv3regs.h

out:
	mkdir -p out

out/%.o: %.S gicv3regs.h
	aarch64-none-elf-gcc $(CFLAGS) -c -o out/$*.o $*.S

out/%.ppu: %.pas gicv3regs.inc
	fpc $(FPCFLAGS) -Cn $*.pas

out/generic.elf: out $(OBJS) $(UNITS)
# for lots of info about the build, add -va
	fpc $(FPCFLAGS) generic.pas $(LINKFLAGS)

clean:
	rm -f out/* gicv3regs.inc gicv3regs.h

out/generic.img: out/generic.elf
	aarch64-none-elf-objcopy -O binary out/generic.elf out/generic.img

run: out/generic.img
	qemu-system-aarch64 $(QEMU_DEFAULT) $(QEMU_ARGS)

debug: out/generic.img
	qemu-system-aarch64 $(QEMU_DEFAULT) -S -s $(QEMU_ARGS)
