﻿unit dtb;

interface

{$rangeChecks on}

uses
    classes;

const
    FDT_BEGIN_NODE = $00000001;
    FDT_END_NODE = $00000002;
    FDT_PROP = $00000003;
    FDT_NOP  = $00000004;
    FDT_END  = $00000009;

    ValidMagic = $D00DFEED;

type
    PDeviceTree = ^TDeviceTree;
    TDeviceTree = packed record
        Magic: UInt32;
        TotalSize: UInt32;
        OffsetDTStruct: UInt32;
        OffsetDTStrings: UInt32;
        OffsetMemResMap: UInt32;
        Version: UInt32;
        LastCompatibleVersion: UInt32;
        BootCPUIDPhys: UInt32;
        SizeDTStrings: UInt32;
        SizeDTStruct: UInt32;
    end;

    PReservationEntry = ^TReservationEntry;
    TReservationEntry = packed record
        Address: UInt64;
        Size: UInt64;
    end;

    PFDTProperty = ^TFDTProperty;
    TFDTProperty = packed record
        Length: UInt32;
        NameOffset: UInt32;
    end;

    PParsedTreeProperty = ^TParsedTreeProperty;
    TParsedTreeProperty = record
        Name: String;
        Value: array of Byte;
    end;

    PParsedTree = ^TParsedTree;
    TParsedTree = record
        Name: String;
        UnitAddress: String;
        Children: array of PParsedTree;
        Parent: PParsedTree;
        Properties: array of PParsedTreeProperty;
    end;

    TPropertyType = (PTStringList, PTString, PTU32, PTU64, PTPHandle, PTPropEncodedArray, PTEmpty, PTByteArray);

    TByteArray = array of Byte;

type
    TDTBParser = class
    private
        BaseAddress: UInt32;
        DTB: TDeviceTree;
        ParsedTree: PParsedTree;

        function VerifyHeader: Boolean;
        function ParseHeader: TDeviceTree;
        procedure ReadTree;
        function ReadString(Address: UInt32): String;
        function ReadProperty(Stream: TCustomMemoryStream): PParsedTreeProperty;
        function PropertyType(Name: String): TPropertyType;
        function PropertyU32(Prop: PParsedTreeProperty): UInt32;
        function PropertyU64(Prop: PParsedTreeProperty): UInt64;
        function PropertyString(Prop: PParsedTreeProperty): String;
        function PropertyByteArray(Prop: PParsedTreeProperty): TByteArray;
    protected

    public
        constructor Create(Base: UInt32);
        destructor Destroy; override;

        property DeviceTree: PParsedTree read ParsedTree write ParsedTree stored false;

        procedure PrettyPrint;

        function GetNode(Name: String): PParsedTree;
        function GetProperty(Node: PParsedTree; Name: String): PParsedTreeProperty;
    published

    end;

implementation

uses sysutils, mmio, utils, MemoryStream;

procedure AlignDword(aStream: TCustomMemoryStream);
var
    toskip: integer;
begin
    toskip := 4 - (aStream.Position mod 4);
    if toskip <> 4 then
    begin
        aStream.Seek(toskip, soFromCurrent);
    end;
end;

{- Devicetree is in big-endian format, and so we'll need to byte swap a lot -}

constructor TDTBParser.Create(Base: UInt32);
begin
    self.BaseAddress := Base;
    self.DTB := ParseHeader;
    ReadTree;
end;


destructor TDTBParser.Destroy;
begin
    inherited;

    if ParsedTree <> Nil then
    begin
        dispose(ParsedTree);
    end;
end;


function TDTBParser.VerifyHeader: Boolean;
begin
    VerifyHeader := BEtoN(GET32(BaseAddress)) = ValidMagic;
end;


function TDTBParser.ParseHeader: TDeviceTree;
var
    d: PDeviceTree;
    Res: TDeviceTree;
begin
    d := Pointer(PtrUInt(BaseAddress));

    {- copy the DTB, we're about to modify it -}
    Res := d^;

    Res.Magic := BEtoN(d^.Magic);
    Res.TotalSize := BEtoN(d^.TotalSize);
    Res.OffsetDTStruct := BEtoN(d^.OffsetDTStruct);
    Res.OffsetDTStrings := BEtoN(d^.OffsetDTStrings);
    Res.OffsetMemResMap := BEtoN(d^.OffsetMemResMap);
    Res.Version := BEtoN(d^.Version);
    Res.LastCompatibleVersion := BEtoN(d^.LastCompatibleVersion);
    Res.BootCPUIDPhys := BEtoN(d^.BootCPUIDPhys);
    Res.SizeDTStrings := BEtoN(d^.SizeDTStrings);
    Res.SizeDTStruct := BEtoN(d^.SizeDTStruct);

    ParseHeader := Res;
end;


function TDTBParser.ReadString(Address: UInt32): String;
var
    R: String = '';
    I: UInt32;
    C: Char;
begin
    I := 0;

    repeat
        C := Char(GET32(Address + I));

        if C <> #0 then
        begin
            R := R + C;
            Inc(I);
        end;
    until C = #0;

    ReadString := R;
end;


function TDTBParser.PropertyType(Name: String): TPropertyType;
begin
    case Name of
        'compatible': begin PropertyType := PTStringList; end;
        'model': begin PropertyType := PTString; end;
        'phandle': begin PropertyType := PTU32; end;
        'status': begin PropertyType := PTString; end;
        '#address-cells': begin PropertyType := PTU32; end;
        '#size-cells': begin PropertyType := PTU32; end;
        'reg': begin PropertyType := PTPropEncodedArray; end;
        'virtual-reg': begin PropertyType := PTU32; end;
        'ranges': begin PropertyType := PTPropEncodedArray; end;
        'dma-ranges': begin PropertyType := PTPropEncodedArray; end;
        'dma-coherent': begin PropertyType := PTEmpty; end;
        'dma-noncoherent': begin PropertyType := PTEmpty; end;
        'name': begin PropertyType := PTString; end;
        'device_type': begin PropertyType := PTString; end;
        'interrupts': begin PropertyType := PTPropEncodedArray; end;
        'interrupt-parent': begin PropertyType := PTPHandle; end;
        '#interrupt-cells': begin PropertyType := PTU32; end;
        'interrupt-controller': begin PropertyType := PTEmpty; end;
        'interrupt-map': begin PropertyType := PTPropEncodedArray; end;
        'interrupt-map-mask': begin PropertyType := PTPropEncodedArray; end;
    else
    begin PropertyType := PTByteArray; end;
    end;
end;


function TDTBParser.PropertyU32(Prop: PParsedTreeProperty): UInt32;
begin
    PropertyU32 := BEtoN(PUInt32(@Prop^.Value[0])^);
end;


function TDTBParser.PropertyU64(Prop: PParsedTreeProperty): UInt64;
begin
    PropertyU64 := BEtoN(PUInt64(@Prop^.Value[0])^);
end;


function TDTBParser.PropertyString(Prop: PParsedTreeProperty): String;
var
    R: String = '';
    B: Byte;
begin
    for B in Prop^.Value do
    begin
        R := R + Char(B);
    end;

    PropertyString := R;
end;


function TDTBParser.PropertyByteArray(Prop: PParsedTreeProperty): TByteArray;
var
    R: TByteArray;
begin
    R := Copy(Prop^.Value, 0);

    PropertyByteArray := R;
end;


function TDTBParser.ReadProperty(Stream: TCustomMemoryStream): PParsedTreeProperty;
var
    Prop: TFDTProperty;
    FinalProp: PParsedTreeProperty;
begin
    Stream.Read(Prop, Sizeof(TFDTProperty));

    FinalProp := new(PParsedTreeProperty);

    FinalProp^.Name := ReadString(BaseAddress + DTB.OffsetDTStrings + BEtoN(Prop.NameOffset));

    SetLength(FinalProp^.Value, BEtoN(Prop.Length));
    Stream.Read(FinalProp^.Value, BEtoN(Prop.Length));

    AlignDword(Stream);

    ReadProperty := FinalProp;
end;


procedure TDTBParser.ReadTree;
var
    R: UInt32 = 0;
    Prop: PParsedTreeProperty;
    CurNode: PParsedTree = Nil;
    TempNode: PParsedTree = Nil;
    C: Char;
    NodeName, UnitAddress: String;
    NodeNameSplitPos: Integer;
    StartAddr, EndAddr: UInt32;
    Stream: TStreamOnStaticMemory;
begin
    if not VerifyHeader then
    begin
        exit;
    end;

    if ParsedTree <> Nil then
    begin
        exit;
    end;

    ParsedTree := new(PParsedTree);

    StartAddr := BaseAddress + DTB.OffsetDTStruct;
    EndAddr := BaseAddress + DTB.OffsetDTStrings;

    Stream := TStreamOnStaticMemory.Create(Pointer(PtrUInt(StartAddr)), EndAddr - StartAddr);

    while Stream.Position < Stream.Size - 1 do
    begin
        R := BEtoN(Stream.ReadDword());

        case R of
            0: begin
            end;
            FDT_NOP: begin
            end;
            FDT_END: begin
                exit;
            end;
            FDT_BEGIN_NODE: begin
                NodeName := '';
                UnitAddress := '';

                repeat
                    C := Char(Stream.ReadByte());

                    if C <> #0 then
                    begin
                        NodeName := NodeName + C;
                    end;
                until C = #0;

                NodeNameSplitPos := Pos('@', NodeName);
                if NodeNameSplitPos > 0 then
                begin
                    UnitAddress := RightStr(NodeName, Length(NodeName) - NodeNameSplitPos);
                    NodeName := LeftStr(NodeName, NodeNameSplitPos - 1);
                end;

                if CurNode = Nil then
                begin
                    // this is the first node in the tree
                    CurNode := new(PParsedTree);
                    CurNode^.Name := '/';
                    CurNode^.UnitAddress := UnitAddress;
                    SetLength(CurNode^.Children, 0);
                    SetLength(CurNode^.Properties, 0);

                    ParsedTree := CurNode;
                end
                else begin
                    TempNode := CurNode;

                    CurNode := new(PParsedTree);
                    if TempNode^.Name = '/' then
                    begin
                        CurNode^.Name := TempNode^.Name + NodeName;
                    end
                    else begin
                        CurNode^.Name := TempNode^.Name + '/' + NodeName;
                    end;
                    CurNode^.UnitAddress := UnitAddress;
                    CurNode^.Parent := TempNode;
                    SetLength(CurNode^.Children, 0);
                    SetLength(CurNode^.Properties, 0);

                    SetLength(TempNode^.Children, Length(TempNode^.Children) + 1);
                    TempNode^.Children[High(TempNode^.Children)] := CurNode;
                end;

                repeat
                    C := Char(Stream.ReadByte());
                until C <> #0;

                if C <> #0 then
                begin
                    Stream.Seek(-4, soFromCurrent);
                end;
            end;
            FDT_END_NODE: begin
                CurNode := CurNode^.Parent;
            end;
            FDT_PROP: begin
                Prop := ReadProperty(Stream);

                if Prop <> Nil then
                begin
                    SetLength(CurNode^.Properties, Length(CurNode^.Properties) + 1);
                    CurNode^.Properties[High(CurNode^.Properties)] := Prop;
                end;
            end;
        end;
    end;
end;


procedure TDTBParser.PrettyPrint;

    procedure Indent(Depth: Integer);
    begin
        if Depth > 0 then
        begin
            Write(PadLeft('  ', Depth));
        end;
    end;

    procedure PrintProperty(Prop: PParsedTreeProperty; Depth: Integer);
    var
        B: Byte;
    begin
        Indent(Depth);
        Write('  ' + Prop^.Name + ' (');
        Write(Length(Prop^.Value));
        Write(' entries) = [');

        case PropertyType(Prop^.Name) of
            PTString: begin
                Write(' "');
                Write(PropertyString(Prop));
                Write('"');
            end;
            PTU32: begin
                Write(' 0x');
                Write(IntToHex(PropertyU32(Prop)));
            end;
            PTU64: begin
                Write(' 0x');
                Write(IntToHex(PropertyU32(Prop)));
            end;
        else
        begin
            for B in Prop^.Value do
            begin
                Write(' 0x');
                Write(IntToHex(B));
            end;
        end;
        end;

        WriteLn(' ]');
    end;

    procedure PrintNode(Node: PParsedTree; Depth: Integer);
    var
        Prop: PParsedTreeProperty;
        N: PParsedTree;
    begin
        Indent(Depth);
        Write('+ ' + Node^.Name);
        if Node^.UnitAddress <> '' then
        begin
            Write(' @ ' + Node^.UnitAddress);
        end;
        WriteLn;
        Indent(Depth);
        Write('  Properties: ');
        Write(Length(Node^.Properties));
        WriteLn(' count');

        for Prop in Node^.Properties do
        begin
            PrintProperty(Prop, Depth);
        end;

        if Length(Node^.Children) > 0 then
        begin
            for N in Node^.Children do
            begin
                PrintNode(N, Depth + 1);
            end;
        end;
    end;

begin
    PrintNode(ParsedTree, 0);
end;

function TDTBParser.GetNode(Name: String): PParsedTree;

    function FindNode(LeafNode: PParsedTree; Name: String): PParsedTree;
    var
        N, CN: PParsedTree;
    begin
        if LeafNode^.Name = Name then
        begin
            FindNode := LeafNode;
        end
        else begin
            for N in LeafNode^.Children do
            begin
                CN := FindNode(N, Name);
                if CN^.Name = Name then
                begin
                    exit(CN);
                end;
            end;
            FindNode := Nil;
        end;
    end;

begin
    GetNode := FindNode(ParsedTree, Name);
end;

function TDTBParser.GetProperty(Node: PParsedTree; Name: String): PParsedTreeProperty;
var
    Prop: PParsedTreeProperty;
begin
    for Prop in Node^.Properties do
    begin
        if Prop^.Name = Name then
        begin
            exit(Prop);
        end;
    end;

    GetProperty := Nil;
end;

end.
