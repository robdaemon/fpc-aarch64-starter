# fpc-aarch64-starter

Free Pascal AArch64 starter project

The goal is to show how to do bare metal programming with Free Pascal
*without* selecting a target board during compilation.

Free Pascal ships with Raspberry Pi 3/4 AArch64 targets, but they have
hardcoded initialization routines.

This code targets the `qemu-aarch64-virt` machine, with custom startup
and linker scripts.

## License

Same license as Free Pascal, GNU General Public License v2.

The GIC driver code comes from the ARM Trusted Firmware source code, itself
released under the BSD 3-Clause License. It's not a direct copy, but a port
of parts of it to Pascal from C.
