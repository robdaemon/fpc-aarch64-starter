﻿program generic ;

{$L vectors.o}
{$L start.o}

{-
 This example targets the qemu virt AArch64 machine. See
 https://www.qemu.org/docs/master/system/arm/virt.html 
 for details.
-}

uses sysutils, consoleio, mmio, uart, exceptions, gicv3, dtb, MemoryStream;

const
    UART0_BASE = $9000000;
    DTB_BASE = $40000000;

function VirtWrite(ACh: AnsiChar; AUserData: pointer): boolean;
begin
    UARTPuts(UART0_BASE, ACh);

    VirtWrite := true;
end;

function VirtRead(var ACh: AnsiChar; AUserData: pointer): boolean;
begin
    ACh := UARTGet(UART0_BASE);

    VirtRead := true;
end;

type
    TDynArray = array of byte;

function ExtractUInt64(A: TDynArray; Offset: Integer): UInt64;
begin
    ExtractUInt64 := BEtoN(PUInt64(@A[Offset])^);
end;

procedure PASCALMAIN; external name 'PASCALMAIN';
var
    pdtb: PParsedTree;
    DTBParser: TDTBParser;
    GICNode: PParsedTree;
    GICRegProperty: PParsedTreeProperty;
    GICDStart: UInt64;
    GICRStart: UInt64;
    MStream: TStreamOnStaticMemory;
    I: Integer;
begin
    UARTInit(UART0_BASE);
    OpenIO(Input, @VirtWrite, @VirtRead, fmInput, nil);
    OpenIO(Output, @VirtWrite, @VirtRead, fmOutput, nil);
    OpenIO(ErrOutput, @VirtWrite, @VirtRead, fmOutput, nil);
    OpenIO(StdOut, @VirtWrite, @VirtRead, fmOutput, nil);
    OpenIO(StdErr, @VirtWrite, @VirtRead, fmOutput, nil);

    WriteLn('Hello world');

    WriteLn;
    WriteLn('Parsing the Device Tree...');
    DTBParser := TDTBParser.Create(DTB_BASE);

    pdtb := DTBParser.DeviceTree;
    Write('Parsed tree has a root named ');
    Write(pdtb^.Name);
    Write(' and ');
    Write(Length(pdtb^.Children));
    WriteLn(' children nodes');

    DTBParser.PrettyPrint;

    GICNode := DTBParser.GetNode('/intc');
    if GICNode <> Nil then
    begin
        WriteLn('Setting up GIC at unit address ' + GICNode^.UnitAddress);
        GICRegProperty := DTBParser.GetProperty(GICNode, 'reg');

        if GICRegProperty <> Nil then
        begin
            GICDStart := ExtractUInt64(GICRegProperty^.Value, 0);
            GICRStart := ExtractUInt64(GICRegProperty^.Value, 16);
            WriteLn('Found GIC reg property');
            Write('GICDStart = 0x');
            WriteLn(IntToHex(GICDStart));
            Write('GICD Length = 0x');
            WriteLn(IntToHex(ExtractUInt64(GICRegProperty^.Value, 8)));
            Write('GICRStart = 0x');
            WriteLn(IntToHex(GICRStart));
            Write('GICR Length = 0x');
            WriteLn(IntToHex(ExtractUInt64(GICRegProperty^.Value, 24)));
            Init_GICv3(GICDStart, GICRStart);
        end;
    end;

    DTBParser.Destroy;

    // DUMMY(2500);
    // WriteLn('Causing an Data Abort by writing outside of bounds');
    // PUT64($ffffffffffff, $1234);

    // WriteLn('Back from the Data Abort');


    WriteLn('Entering infinite loop');

    while True do
    begin
    end;
end.
