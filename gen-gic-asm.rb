#!/usr/bin/env ruby

require "erb"

template = %q{
% registers.each do |r|
  % if r.start_with?("#")
    % next
  % end
  % register, rw = r.split(",")
  % if rw == "rw"
    read_write_register <%= register %>
  % elsif rw == "wo"
    write_register <%= register %>
  % else
    read_register <%= register %>
  % end
% end
}.gsub(/^\s+/, '')

source = ERB.new(template, trim_mode: "%<>")

registers = STDIN.read.split("\n")
puts source.result
