﻿unit exceptions;

interface

procedure Current_EL_SP0_Sync; public name '_fpc_curr_el_sp0_sync';
procedure Current_EL_SP0_IRQ; public name '_fpc_curr_el_sp0_irq';
procedure Current_EL_SP0_FIQ; public name '_fpc_curr_el_sp0_fiq';
procedure Current_EL_SP0_SError; public name '_fpc_curr_el_sp0_serror';
procedure Current_EL_SPX_Sync; public name '_fpc_curr_el_spx_sync';
procedure Current_EL_SPX_IRQ; public name '_fpc_curr_el_spx_irq';
procedure Current_EL_SPX_FIQ; public name '_fpc_curr_el_spx_fiq';
procedure Current_EL_SPX_SError; public name '_fpc_curr_el_spx_serror';
procedure Lower_EL_AArch64_Sync; public name '_fpc_lower_el_aarch64_sync';
procedure Lower_EL_AArch64_IRQ; public name '_fpc_lower_el_aarch64_irq';
procedure Lower_EL_AArch64_FIQ; public name '_fpc_lower_el_aarch64_fiq';
procedure Lower_EL_AArch64_SError; public name '_fpc_lower_el_aarch64_serror';
procedure Lower_EL_AArch32_Sync; public name '_fpc_lower_el_aarch32_sync';
procedure Lower_EL_AArch32_IRQ; public name '_fpc_lower_el_aarch32_irq';
procedure Lower_EL_AArch32_FIQ; public name '_fpc_lower_el_aarch32_fiq';
procedure Lower_EL_AArch32_SError; public name '_fpc_lower_el_aarch32_serror';

implementation

procedure Current_EL_SP0_Sync;
begin
    WriteLn('Current_EL_SP0_Sync');
end;

procedure Current_EL_SP0_IRQ;
begin
    WriteLn('Current_EL_SP0_IRQ');
end;

procedure Current_EL_SP0_FIQ;
begin
    WriteLn('Current_EL_SP0_FIQ');
end;

procedure Current_EL_SP0_SError;
begin
    WriteLn('Current_EL_SP0_SError');
end;

procedure Current_EL_SPX_Sync;
begin
    WriteLn('Current_EL_SPX_Sync');

    while True do
    begin
    end;
end;

procedure Current_EL_SPX_IRQ;
begin
    WriteLn('Current_EL_SPX_IRQ');
end;

procedure Current_EL_SPX_FIQ;
begin
    WriteLn('Current_EL_SPX_FIQ');
end;

procedure Current_EL_SPX_SError;
begin
    WriteLn('Current_EL_SPX_SError');
end;

procedure Lower_EL_AArch64_Sync;
begin
    WriteLn('Lower_EL_AArch64_Sync');
end;

procedure Lower_EL_AArch64_IRQ;
begin
    WriteLn('Lower_EL_AArch64_IRQ');
end;

procedure Lower_EL_AArch64_FIQ;
begin
    WriteLn('Lower_EL_AArch64_FIQ');
end;

procedure Lower_EL_AArch64_SError;
begin
    WriteLn('Lower_EL_AArch64_SError');
end;

procedure Lower_EL_AArch32_Sync;
begin
    WriteLn('Lower_EL_AArch32_Sync');
end;

procedure Lower_EL_AArch32_IRQ;
begin
    WriteLn('Lower_EL_AArch32_IRQ');
end;

procedure Lower_EL_AArch32_FIQ;
begin
    WriteLn('Lower_EL_AArch32_FIQ');
end;

procedure Lower_EL_AArch32_SError;
begin
    WriteLn('Lower_EL_AArch32_SError');
end;

end.
